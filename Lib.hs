module Lib where

import System.Random
import Data.List

rand :: [ a ] -> IO a
rand xs = do
  gen <- newStdGen
  let (i, _) = randomR (0, length xs - 1) gen
  return (xs !! i)

randList :: Int -> [ a ] -> IO [ a ]
randList n xs = do
  gen <- newStdGen
  let is = take n . nub $ randomRs (0, length xs - 1) gen :: [ Int ]
  return (map (\ i -> xs !! i) is)
