module Init where

import System.Random
import Data.List
import Data.Either
import Text.CSV
import Data.Char
import Lib
import Course
import Group
import Professor
import Room
import TimeSlot
import School
import Individual
import Population

type FileContents = [ [ Int ] ]
type Files =  [ (String, String) ]

files :: Files
files = [ ("course", "./data/course.csv")
	, ("group", "./data/group.csv")
        , ("professor", "./data/professor.csv")
        , ("room", "./data/room.csv")
	, ("timeSlot", "./data/timeSlot.csv") ]

parseFile :: FilePath -> IO CSV
parseFile = fmap (either (error . show) (tail . init)) . parseCSVFromFile

fillSchool :: IO School
fillSchool = 
  let findFile name = snd $ head $ filter (\ (n, _) -> n == name) files in
  do 
    csvCourse <- parseFile $ findFile "course"
    csvGroup <- parseFile $ findFile "group"
    csvProfessor <- parseFile $ findFile "professor"
    csvRoom <- parseFile $ findFile "room"
    scvTimeSlot <- parseFile $ findFile "timeSlot"
    let school = newSchool (roomsList csvRoom)
                           (professorsList csvProfessor)
	                   (coursesList csvCourse)
                           (groupsList csvGroup)
			   (timeSlotList scvTimeSlot)
    return school

coursesList :: CSV -> [ Course ]
coursesList = 
  map (\ c -> newCourse (read (head c)) (c !! 1) (c !! 2) (map read $ drop 3 c))

groupsList :: CSV -> [ Group ]
groupsList = 
  map (\ g -> newGroup (read (head g)) (read (g !! 1)) (map read $ drop 2 g))

professorsList :: CSV -> [ Professor ]
professorsList = 
  map (\ p -> newProfessor (read (head p)) (p !! 1))

roomsList :: CSV -> [ Room ]
roomsList = 
  map (\ r -> newRoom (read (head r)) (r !! 1) (read (r !! 2)))

timeSlotList :: CSV -> [ TimeSlot ] 
timeSlotList = 
  map (\ t -> newTimeSlot (read (head t)) (t !! 1))

createPopulation :: Int -> School -> IO Population
createPopulation 0 _ = return []
createPopulation size s = do
  c <- createChromosome s
  let individual = newIndividual (zip [ 0 .. ] c) Nothing 
  rest <- createPopulation (size-1) s
  return (individual : rest)
