module Professor where

data Professor = Professor { id :: Int
                           , name :: String } deriving (Show, Eq)

newProfessor :: Int -> String -> Professor
newProfessor i n = Professor { Professor.id = i
                             , name = n }
