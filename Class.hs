module Class where

data Class = Class { id :: Int
                   , group :: Int
		   , course :: Int
		   , professor :: Int
		   , timeSlot :: Int 
		   , room :: Int } deriving (Show, Eq)

newClass :: Int -> Int -> Int -> Int -> Int -> Int -> Class
newClass i g c p t_s r = 
  Class { Class.id = i
        , group = g
	, course = c 
	, professor = p
	, timeSlot = t_s
	, room = r}
