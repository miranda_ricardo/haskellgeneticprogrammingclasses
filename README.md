(C) Ricardo Cristovao Miranda, 2016.

This program is a Haskell solution to the problem described in Genetic Algorithms in 
Java Basics, Lee Jacobson, Burak Kanber, chapter 5, Class Scheduling.
From the book:
The class-scheduling problem we will be looking at in this chapter is a college class 
scheduler that can create a college timetable based on data we provide it, such as the 
available professors, available rooms, timeslots and student groups.
Each class will be assigned a timeslot, a professor, a room and a student group by the 
class scheduler. We can calculate the total number of classes that will need scheduling 
by summing the number of student groups multiplied by the number of modules each 
student group is enrolled in.
For each class scheduled by our application we will consider the following hard For 
each class scheduled by our application we will consider the following hard constraints:
  - Classes can only be scheduled in free classrooms
  - A professor can only teach one class at any one time
  - Classrooms must be big enough to accommodate the student group
We can simply assign a numerical ID to each timeslot, professor and classroom. We can 
then use a chromosome that encodes an array of integers - our familiar approach. This 
means each class that needs scheduling will only require three integers to encode it.
By splitting this array into chucks of three, we can retrieve all of the information 
we need for each class.
